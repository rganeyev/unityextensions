﻿using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof (SetPlayerPrefsOnClick))]
public class SetPlayerPrefsOnClickEditor : Editor {
    public override void OnInspectorGUI() {
        var myTarget = (SetPlayerPrefsOnClick) target;

        myTarget.prefsKey = EditorGUILayout.TextField("PlayerPrefs key", myTarget.prefsKey);
        myTarget.prefsType = (PlayerPrefsType) EditorGUILayout.EnumPopup("PlayerPrefs type", myTarget.prefsType);
        switch (myTarget.prefsType) {
            case PlayerPrefsType.intValue:
                myTarget.intValue = EditorGUILayout.IntField("Value", myTarget.intValue);
                break;
            case PlayerPrefsType.boolValue:
                myTarget.boolValue = EditorGUILayout.Toggle("Value", myTarget.boolValue);
                break;
            case PlayerPrefsType.stringValue:
                myTarget.stringValue = EditorGUILayout.TextField("Value", myTarget.stringValue);
                break;
        }
    }
}