﻿using System.Collections.Generic;
using System.IO;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEditor;
using UnityEngine;

public static class CustomMenuExtensions {
    [MenuItem("LaikaBOSS/Clear PlayerPrefs")]
    public static void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        ObscuredPrefs.DeleteAll();
        Debug.Log("Cleared player prefs");
    }

    [MenuItem("Custom/Game Object/Revert selected to prefab")]
    public static void RevertSelectedPrefabs()
    {
        var gos = Selection.gameObjects;
        foreach (var go in gos) {
            if (PrefabUtility.RevertPrefabInstance(go))
                Debug.Log("Reverted to prefab state successfully: " + go.name, go);
            else
                Debug.LogError("Couldn't revert to a prefab state: " + go.name, go);
        }
    }

    [MenuItem("Custom/Game Object/Reconnect selected to prefab")]
    public static void ReconnectSelectedToPrefabs()
    {
        var gos = Selection.gameObjects;
        foreach (var go in gos) {
            PrefabUtility.ReconnectToLastPrefab(go);
        }
    }

    /// Screenshots
    [MenuItem("Custom/Take mobile screenshots")]
    public static void Take()
    {
        var dimensions = new List<Vector2>();

        //dimensions.Add(new Vector2(640, 960));
        //dimensions.Add(new Vector2(640, 1136));

        //dimensions.Add(new Vector2(768, 1024));
        //dimensions.Add(new Vector2(768, 1280));
		dimensions.Add(new Vector2(500, 1024));
		dimensions.Add(new Vector2(120, 180));
        //dimensions.Add(new Vector2(1080, 1920));
        //dimensions.Add(new Vector2(1242, 2208));
        //dimensions.Add(new Vector2(1536, 2048));

        var scr = new GameObject("takeScreenshot", typeof (ScreenshotTaker)).GetComponent<ScreenshotTaker>();

        if (scr != null)
            scr.Take(dimensions, false);
    }

    [MenuItem("Custom/Take instant screenshot")]
    public static void TakeInstantScreenshot()
    {
        var path = Path.Combine(Application.dataPath.SubstringToOccurence("Assets"), "instant");
		if (File.Exists(path + ".png")) {
			for (int i = 1;; ++i) {
				string temp = path + "_" + i + ".png";
				if (!File.Exists(temp)) {
					path = temp;
					break;
				}
			}
		} else path = path + ".png";


        Application.CaptureScreenshot(path);
        ContinuationManager.Add(() => File.Exists(path), () => Debug.Log("Took screenshot at " + path));
    }
}