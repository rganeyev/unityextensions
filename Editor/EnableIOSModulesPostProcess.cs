﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
#if (UNITY_5 && UNITY_IOS)
using UnityEditor.iOS.Xcode;
#endif

	//A simple, shared PostProcessBuildPlayer script to enable Objective-C modules. This lets us add frameworks
	//from our source files, rather than through modifying the Xcode project. 

public class EnableIOSModulesPostProcess
{

    [PostProcessBuild(1500)] // We should try to run last
    public static void OnPostProcessBuild(BuildTarget target, string path)
    {
#if (UNITY_5 && UNITY_IOS)
#if UNITY_5
		if (target != BuildTarget.iOS) {
#elif UNITY_4_6 || UNITY_4_7
        if (target != BuildTarget.iPhone) {
#endif
			return; // How did we get here?
		}

		UnityEngine.Debug.Log("PostPROCESS: Enabling Objective-C modules");
		string pbxproj = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

		// Looking for the buildSettings sections of the pbxproj
		string insertKeyword = "buildSettings = {";
		string foundKeyword = "CLANG_ENABLE_MODULES"; // for checking if it's already inserted
		string modulesFlag = "				CLANG_ENABLE_MODULES = YES;";

		List<string> lines = new List<string>();
			
		foreach (string str in File.ReadAllLines(pbxproj)) {
			if (!str.Contains(foundKeyword)) { 
				lines.Add(str);
			}
			if (str.Contains(insertKeyword)) {
				lines.Add(modulesFlag);
			}
		}
			
		// Clear the file		
		using (File.Create(pbxproj)) {}

		foreach (string str in lines) {
			File.AppendAllText(pbxproj, str + Environment.NewLine);
		}

        UpdateTransportSecurityAndFullScreen(target, path);
#endif
    }

#if (UNITY_5 && UNITY_IOS)
    private static void UpdateTransportSecurityAndFullScreen(BuildTarget target, string path) {
        string plistPath = path + "/Info.plist";
        PlistDocument plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));

        PlistElementDict rootDict = plist.root;
        var transportSecurityKey = "NSAppTransportSecurity";
        PlistElementDict element;
        if (rootDict.values.ContainsKey(transportSecurityKey))
            element = rootDict[transportSecurityKey].AsDict();
        else 
            element = rootDict.CreateDict(transportSecurityKey);    
        element.SetBoolean("NSAllowsArbitraryLoads", true);
        rootDict.SetBoolean("UIRequiresFullScreen", true);

        // Write to file
        File.WriteAllText(plistPath, plist.WriteToString());
    }
#endif
}