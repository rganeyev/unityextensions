using UnityEngine;
using System.Collections;
using System.Linq;

namespace ME {

	public class MEMath {

		public class Bounds {
			
			public struct HitInfo {
				
				public bool xIntersection;
				public bool yIntersection;
				public bool zIntersection;
				
				public Vector3 collisionPoint;
				
			}

			private HitInfo hitInfo;

			public Vector3 center;

			private Vector3 _size;
			private Quaternion _rotation;
			private Vector3 _eulerAngles;
			private Vector3 _extents;

#if UNITY_EDITOR
			public bool gizmos = true;
#endif

			public Vector3 size {
				get { return this._size; }
				set { this._size = value; this.extents = this.size * 0.5f; }
			}
			public Quaternion rotation {
				get { return this._rotation; }
				set { this._rotation = value; this._eulerAngles = this.rotation.eulerAngles; }
			}
			public Vector3 eulerAngles {
				get { return this._eulerAngles; }
				set { this._eulerAngles = value; this._rotation = Quaternion.Euler(this.eulerAngles); }
			}
			public Vector3 extents {
				get { return this._extents; }
				private set { this._extents = value; }
			}
			
			public Bounds() {}
			
			public Bounds(Vector3 center, Vector3 size, Quaternion rotation) {
				
				this.center = center;
				this.size = size;
				this.rotation = rotation;
				
				this.eulerAngles = rotation.eulerAngles;

#if UNITY_EDITOR
				this.gizmos = true;
#endif
			}
			
			public Bounds(Vector3 center, Vector3 size, Vector3 eulerAngles) {
				
				this.center = center;
				this.size = size;
				this.rotation = Quaternion.Euler(eulerAngles);
				
				this.eulerAngles = eulerAngles;
				this.extents = this.size / 2f;
				
#if UNITY_EDITOR
				this.gizmos = false;
#endif

			}
			
			public void Rotate(Quaternion rotation) {
				
				this.rotation *= rotation;
				this.eulerAngles = this.rotation.eulerAngles;
				
			}
			
			public void Rotate(Vector3 eulerAngles) {
				
				this.eulerAngles += eulerAngles;
				this.rotation = Quaternion.Euler(this.eulerAngles);
				
			}
			
			public bool Contains(Vector3 point) {
				
				Quaternion rotation = Quaternion.Inverse(this.rotation);
				Vector3 checkPoint = rotation * (point - this.center);
				
				if (
					checkPoint.x <= this.extents.x && checkPoint.y <= this.extents.y && checkPoint.z <= this.extents.z &&
					checkPoint.x >= -this.extents.x && checkPoint.y >= -this.extents.y && checkPoint.z >= -this.extents.z
					) {
					
					return true;
					
				}
				
				return false;
				
			}

			private Vector3[] points;
			public Vector3[] GetPoints() {

				if (this.points == null) this.points = new Vector3[8];
				
				this.points[0] = this.center + this.rotation * this.extents;
				this.points[1] = this.center + this.rotation * new Vector3(this.extents.x, this.extents.y, -this.extents.z);
				this.points[2] = this.center + this.rotation * new Vector3(-this.extents.x, this.extents.y, -this.extents.z);
				this.points[3] = this.center + this.rotation * new Vector3(-this.extents.x, this.extents.y, this.extents.z);

				this.points[4] = this.center + this.rotation * new Vector3(this.extents.x, -this.extents.y, this.extents.z);
				this.points[5] = this.center + this.rotation * new Vector3(this.extents.x, -this.extents.y, -this.extents.z);
				this.points[6] = this.center + this.rotation * -this.extents;
				this.points[7] = this.center + this.rotation * new Vector3(-this.extents.x, -this.extents.y, this.extents.z);

				return this.points;

			}

			private float GetVector3SideLength(ref Vector3 v) {

				return v.x + v.y + v.z;

			}

			public void GetProjection(Vector3 axis, Quaternion rotation, out float min, out float max, Color color) {

				// Прямая представляет из себя минимальную и максимальную точку на оси axis
				min = float.MaxValue;
				max = float.MinValue;

				Vector3 n;
				float val;
				foreach (var point in this.points) {

					n = point;
					n.Scale(axis);
					val = this.GetVector3SideLength(ref n);
					
					if (val < min) min = val;
					if (val > max) max = val;

#if UNITY_EDITOR
					if (this.gizmos == true) Gizmos.DrawSphere(point, 0.2f);
#endif
				}
				
#if UNITY_EDITOR
				Debug.DrawLine(axis * min, axis * min + Vector3.up * 2, Color.white);
				Debug.DrawLine(axis * max, axis * max + Vector3.up * 2, Color.cyan);
				Debug.DrawLine(axis * min, axis * max, color);
#endif
			}

			private bool ProjectionIntersects(Vector3 axis, MEMath.Bounds other, Color color) {
				
				var line1Start = 0f;
				var line2Start = 0f;
				var line1End = 0f;
				var line2End = 0f;

				this.GetProjection(axis, this.rotation, out line1Start, out line1End, color);
				other.GetProjection(axis, this.rotation, out line2Start, out line2End, color);

				// Line1 out of Line2
				if ((line1End < line2Start || line1Start > line2End)) {

					return false;

				}

				return true;

			}

			private float ProjectionIntersectsValue(Vector3 axis, MEMath.Bounds other, Color color, bool needDebug = false) {
				
				var line1Start = 0f;
				var line2Start = 0f;
				var line1End = 0f;
				var line2End = 0f;
				
				this.GetProjection(axis, this.rotation, out line1Start, out line1End, color);
				other.GetProjection(axis, this.rotation, out line2Start, out line2End, color);
				
				// Line1 out of Line2
				if ((line1End < line2Start || line1Start > line2End)) {

					//if (needDebug)
					//	Debug.LogWarning("line1Start: " + line1Start + " line1End: " + line1End + "line2Start: " + line2Start + " line2End: " + line2End);
					float a = Mathf.Abs(line2Start - line1End);
					float b = Mathf.Abs(line1Start - line2End);
					return Mathf.Min(a, b);
				}
				
				return -1.0f;
			}
			
			public Vector3 GetNearestPoint(Vector3 toPoint) {
				
				Quaternion rotation = Quaternion.Inverse(this.rotation);
				Vector3 checkPoint = rotation * (toPoint - this.center);
				
				checkPoint.x = Mathf.Clamp(checkPoint.x, -this.extents.x, this.extents.x);
				checkPoint.y = Mathf.Clamp(checkPoint.y, -this.extents.y, this.extents.y);
				checkPoint.z = Mathf.Clamp(checkPoint.z, -this.extents.z, this.extents.z);
				
				return this.rotation * checkPoint + this.center;
				
			}
			
			private bool Intersects_INTERNAL(MEMath.Bounds other, bool withHitInfo = true) {
				
				this.points = this.GetPoints();
				other.points = other.GetPoints();

				var xAxis = this.rotation * new Vector3(1f, 0f, 0f);
				var yAxis = this.rotation * new Vector3(0f, 1f, 0f);
				var zAxis = this.rotation * new Vector3(0f, 0f, 1f);
				
				// Проецируем крайние точки для каждой оси на соответствующие плоскости
#if UNITY_EDITOR
				if (this.gizmos == true) Gizmos.color = Color.red;
#endif
				this.hitInfo.xIntersection = this.ProjectionIntersects(xAxis, other, Color.red); // x
				
				//if (withHitInfo == false && this.hitInfo.xIntersection == false) return false;
				
#if UNITY_EDITOR
				if (this.gizmos == true) Gizmos.color = Color.green;
#endif
				this.hitInfo.yIntersection = this.ProjectionIntersects(yAxis, other, Color.green); // y
				
				//if (withHitInfo == false && this.hitInfo.yIntersection == false) return false;
				
#if UNITY_EDITOR
				if (this.gizmos == true) Gizmos.color = Color.blue;
#endif
				this.hitInfo.zIntersection = this.ProjectionIntersects(zAxis, other, Color.blue); // z
				
				//if (withHitInfo == false && this.hitInfo.zIntersection == false) return false;
				
				if (withHitInfo == true) {
					
					this.hitInfo.collisionPoint = this.GetNearestPoint(other.center);
					
				}
				
				// Получили линии на каждой оси
				// Если хотя бы одна пара линий не накладывается - пересечения нет
				if (!this.hitInfo.xIntersection || !this.hitInfo.yIntersection || !this.hitInfo.zIntersection) {
					
					return false;
					
				}
				
				return true;

			}

			
			public bool Intersects(MEMath.Bounds other) {
				
				var intersects = other.Intersects_INTERNAL(this, false);
				if (intersects == false) return false;

				return this.Intersects_INTERNAL(other, false);

			}

			public Vector3 Intersects_values(MEMath.Bounds other) {
				
				Vector3 my = other.Intersects_VALUES(this);
				Vector3 others = this.Intersects_VALUES(other);

				Vector3 result = Vector3.zero;
				result.x = Mathf.Max(my.x, others.x);
				result.y = Mathf.Max(my.y, others.y);
				result.z = Mathf.Max(my.z, others.z);

				return result;
							}

			public bool Intersects(MEMath.Bounds other, out HitInfo hitInfo) {
				
				this.hitInfo = new HitInfo();

				var intersects = other.Intersects_INTERNAL(this);
				hitInfo = other.GetHitInfo();
				if (intersects == false) return false;

				var result = this.Intersects_INTERNAL(other);
				hitInfo = this.GetHitInfo();

				return result;

			}

			public HitInfo GetHitInfo() {

				return this.hitInfo;

			}

			private Vector3 Intersects_VALUES( MEMath.Bounds other) {
				
				this.points = this.GetPoints();
				other.points = other.GetPoints();
				
				var xAxis = this.rotation * new Vector3(1f, 0f, 0f);
				var yAxis = this.rotation * new Vector3(0f, 1f, 0f);
				var zAxis = this.rotation * new Vector3(0f, 0f, 1f);
				
				// Проецируем крайние точки для каждой оси на соответствующие плоскости
				Vector3 result = Vector3.zero;

				result.x = this.ProjectionIntersectsValue(xAxis, other, Color.red); // x
				result.y = this.ProjectionIntersectsValue(yAxis, other, Color.green); // y
				result.z = this.ProjectionIntersectsValue(zAxis, other, Color.blue); // z
				
				return result;
			}

		}
		
	}

}
