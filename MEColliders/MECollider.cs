using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ME {

	public class MECollider : MonoBehaviour {

		public System.Action<MECollider, MEMath.Bounds.HitInfo> OnCollision;
		
		public ME.MECollider waitForObjectCollision;
		public System.Action<ME.MECollider> eventOnObjectCollision;
		
		public bool additionalCenterActive = false;
		public Vector3 center;

		public bool additionalSizeActive = false;
		public Vector3 size;

		public bool additionalRotationActive = false;
		public Vector3 rotation;

		private MEMath.Bounds _bounds;

		public void Start() {
			
			this.CalculateBounds_INTERNAL();
		}
		
		public void OnCollide(MECollider other, MEMath.Bounds.HitInfo hitInfo) {

			if (this.OnCollision != null) this.OnCollision(other, hitInfo);

		}
		
		public void FixedUpdate() {
			
			if (this.waitForObjectCollision != null) {
				
				ME.MEMath.Bounds.HitInfo hitInfo;
				if (this.waitForObjectCollision.Intersects(this, out hitInfo) == true) {
					
					if (this.eventOnObjectCollision != null) this.eventOnObjectCollision(this);
					
				}
				
			}
			
		}
		
		public ME.MEMath.Bounds GetBounds() {
			
			if (this._bounds == null) this._bounds = new ME.MEMath.Bounds();
			
			if (this.additionalCenterActive == true) {

				this._bounds.center = this.transform.localToWorldMatrix.MultiplyPoint3x4(this.center);

			} else {
				
				this._bounds.center = this.transform.position;
				
			}

			if (this.additionalSizeActive == true) {

				var scale = this.transform.localScale;
				if (this.size != Vector3.one) scale.Scale(this.size);
				this._bounds.size = scale;

			} else {

				this._bounds.size = this.transform.localScale;

			}

			if (this.additionalRotationActive == true) {
				
				this._bounds.eulerAngles = this.transform.eulerAngles + this.rotation;

			} else {

				this._bounds.rotation = this.transform.rotation;

			}
			
#if UNITY_EDITOR
			this._bounds.gizmos = false;
#endif
			return this._bounds;

		}

		private void CalculateBounds_INTERNAL() {

			this._bounds = new ME.MEMath.Bounds();
			this._bounds = this.GetBounds();

		}

		public bool Intersects(MECollider other) {

			return this.GetBounds().Intersects(other.GetBounds());

		}

		public bool Intersects(MECollider other, out MEMath.Bounds.HitInfo hitInfo) {

			return this.GetBounds().Intersects(other.GetBounds(), out hitInfo);

		}

		public void OnDrawGizmos() {

			Gizmos.color = Color.yellow;

			var curBounds = this.GetBounds();

			Matrix4x4 rotationMatrix = Matrix4x4.TRS(curBounds.center, curBounds.rotation, Vector3.one);
			Gizmos.matrix = rotationMatrix;

			Gizmos.DrawWireCube(Vector3.zero, curBounds.size);

		}

	}

}
