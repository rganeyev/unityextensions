﻿using System;

public class Result<T>
{
	public T value;
	public string error;
	
	public bool HasError {
		get { return !string.IsNullOrEmpty (error) || value == null; }
	}
	
	public Result (T value, string error = null)
	{
		this.value = value;
		this.error = error;
	}
	
	private Result (string error)
	{
		this.error = error;
	}
	
	public static Result<T> RaiseError (string error)
	{
		return new Result<T> (error);
	}

#pragma warning disable once 0693
	public bool HandleError<T> (Action<Result<T>> callback)
	{
		if (HasError) {
			callback.CallCallback (Result<T>.RaiseError (error));
			return true;
		}
		
		return false;
	}
}