﻿using System.Collections;
using UnityEngine;
using LaikaBOSS.Constants;

public class Loading : MonoBehaviourExt
{
    public static string sceneName;

    public float time = 1f;

    public static void LoadScene(string scene)
    {
		sceneName = scene;
        LoadLast();
    }

    public static void LoadLast()
    {
        Time.timeScale = 1;
#if UNITY_5
        UnityEngine.SceneManagement.SceneManager.LoadScene(Scenes.LOADING);
#else
        Application.LoadLevel(Scenes.LOADING);
#endif
    }

    protected virtual void Start()
    {
		Time.timeScale = 1;
        WaitAndCall(time, LoadNow);
    }

	public void LoadNow()
	{
#if UNITY_5
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);
#else
        Application.LoadLevelAsync(sceneName);
#endif
    }
}