﻿using UnityEngine;

public class SetPlayerPrefsOnClick : MonoBehaviour {
    public string prefsKey = "";
    public PlayerPrefsType prefsType;

    public int intValue = 1;
    public bool boolValue = true;
    public string stringValue = "";

    public void OnClick() {
        switch (prefsType) {
            case PlayerPrefsType.intValue:
                PlayerPrefs.SetInt(prefsKey, intValue);
                break;
            case PlayerPrefsType.boolValue:
                PlayerPrefs.SetInt(prefsKey, boolValue ? 1 : 0);
                break;
            case PlayerPrefsType.stringValue:
                PlayerPrefs.SetString(prefsKey, stringValue);
                break;
        }
        PlayerPrefs.SetInt(prefsKey, intValue);
    }
}

public enum PlayerPrefsType {
    intValue = 0,
    boolValue,
    stringValue
}