﻿using UnityEngine;

public class OpenURL : MonoBehaviour {
	public string url;

	public void OnClick() {
		Application.OpenURL(url);
	}
}
