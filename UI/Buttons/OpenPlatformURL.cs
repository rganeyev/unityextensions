﻿using UnityEngine;
using System.Collections;

public class OpenPlatformURL : MonoBehaviour {
	public string iosUrl;
	public string androidUrl;

	public void OnClick() {
		if (UnityExtensions.platform == RuntimePlatform.Android) {
			Application.OpenURL(androidUrl);
		    return;
		}

		if (UnityExtensions.platform == RuntimePlatform.IPhonePlayer) {
			Application.OpenURL(iosUrl);
		    return;
		}

		Debug.LogWarning("platform is not defined: " + UnityExtensions.platform, gameObject);
	}

}
