﻿using System.Collections.Generic;
using UnityEngine;

public class DynamicScrollView : MonoBehaviour
{
	public ScrollViewDelegate scrollDelegate;
	public bool initOnStart;

	private void Start ()
	{
		if (initOnStart)
			Init ();
	}

	public readonly List<GameObject> currentItems = new List<GameObject> ();

	public void Clear ()
	{
		foreach (var item in currentItems)
			Destroy (item.gameObject);

		currentItems.Clear ();
	}

	[ContextMenu ("Init scroll")]
	public void Init ()
	{
		Clear ();
		int count = scrollDelegate.Count;
		for (int index = 0; index < count; ++index) {
			var obj = scrollDelegate.ObjectAtIndex (index);
			obj.SetParent (gameObject);
			currentItems.Add (obj);
		}
	}

	public GameObject GetObjectAt (int index)
	{
		if (currentItems == null || currentItems.Count == 0)
			Init ();
		return currentItems [index];
	}

	public int Count {
		get {
			if (currentItems == null || currentItems.Count == 0)
				Init ();
			return currentItems.Count;
		}
	}
}