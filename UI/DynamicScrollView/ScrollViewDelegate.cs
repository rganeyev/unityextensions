﻿using UnityEngine;
using System.Collections;

public abstract class ScrollViewDelegate : MonoBehaviour {

    public abstract int Count { get; }
    public abstract GameObject ObjectAtIndex(int index);

}
