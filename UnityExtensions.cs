﻿#if UNITY_EDITOR
using UnityEditor;
#endif
// ReSharper disable once RedundantUsingDirective
using System;
using System.Collections;
using UnityEngine;

public static class UnityExtensions {
    public static float DPI {
        get {
#if UNITY_ANDROID
            var activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            var activity = activityClass.GetStatic<AndroidJavaObject>("currentActivity");

            var metrics = new AndroidJavaObject("android.util.DisplayMetrics");
            activity.Call<AndroidJavaObject>("getWindowManager")
                .Call<AndroidJavaObject>("getDefaultDisplay")
                .Call("getMetrics", metrics);

            return (metrics.Get<float>("xdpi") + metrics.Get<float>("ydpi"))*0.5f;
#else
            return Screen.dpi;
#endif
        }
    }

    public static RuntimePlatform platform {
        get {
#if UNITY_EDITOR
            switch (EditorUserBuildSettings.activeBuildTarget) {
                case BuildTarget.Android:
                    return RuntimePlatform.Android;
#if UNITY_5
			case UnityEditor.BuildTarget.iOS:
#elif UNITY_4_6 || UNITY_4_7
                case BuildTarget.iPhone:
#endif
                    return RuntimePlatform.IPhonePlayer;
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    return RuntimePlatform.WindowsPlayer;
                case BuildTarget.StandaloneOSXUniversal:
                case BuildTarget.StandaloneOSXIntel64:
                case BuildTarget.StandaloneOSXIntel:
                    return RuntimePlatform.OSXPlayer;
                default:
                    return Application.platform;
            }
#else
			return Application.platform;
#endif
        }
    }

    public static bool IsAppInstalled(string bundleID)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");
		AndroidJavaObject launchIntent = null;

		try {
			launchIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage",bundleID);             
		} catch(Exception ex) {
			return false;
		}         
		return launchIntent != null;         
#else
        return false;
#endif
    }

    public static IEnumerator WaitForRealSeconds(float delay)
    {
        float start = Time.realtimeSinceStartup;
        while (Time.realtimeSinceStartup < start + delay) {
            yield return null;
        }
    }
}