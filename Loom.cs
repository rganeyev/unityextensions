﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

public class Loom : MonoBehaviour
{
	public delegate void WWWCallback (Result<string> result);
	
	public static int maxThreads = 4;
	private static int numThreads;
	private static Loom instance;
	private int count;
	
	public static Loom Instance {
		get {
			if (instance == null)
				instance = FindObjectOfType<Loom> ();
			
			return instance;
		}
	}
	
	public static void Download (string url, Action<WWW> callback, IEnumerable<KeyValuePair<string, string>> parameters = null)
	{
		Instance.StartCoroutine(Instance.DownloadIEnumerator(url, callback, parameters));
	}
	
	public static void DownloadImage(string url, Action<Texture> callback) {
		if (PlayerPrefs.HasKey(url) && File.Exists(PlayerPrefs.GetString(url))) {
			var img = File.ReadAllBytes(PlayerPrefs.GetString(url));
			var texture = new Texture2D(2, 2);
			texture.LoadImage(img);
			callback.CallCallback(texture);
		} else {
			Instance.StartCoroutine(Instance.DownloadIEnumerator(url, www => {
				var texture = www.texture;
				var bytes = texture.EncodeToPNG();
				if (string.IsNullOrEmpty(www.error) && bytes != null && bytes.Length > 0) {
					var path = Path.Combine(Application.persistentDataPath, Math.Abs(texture.GetHashCode()).ToString() + ".png");
					File.WriteAllBytes(path, bytes);
					PlayerPrefs.SetString(url, path);
				}
				callback.CallCallback(texture);
			}));
		}
	}
	
	public static void Download (string url, IEnumerable<KeyValuePair<string, string>> parameters,
	                             WWWCallback callback)
	{
		Instance.StartCoroutine (Instance.DownloadIEnumerator (url, parameters, callback));
	}
	
	private IEnumerator DownloadIEnumerator (string url, Action<WWW> callback, IEnumerable<KeyValuePair<string, string>> parameters = null)
	{
		WWWForm form = null;
		if (parameters != null) {
			form = new WWWForm ();
			foreach (var parameter in parameters)
				form.AddField (parameter.Key, parameter.Value);
		}
		var www = form == null ? new WWW (url) : new WWW (url, form);
		yield return www;
		callback.CallCallback (www);
	}
	
	private IEnumerator DownloadIEnumerator (string url, IEnumerable<KeyValuePair<string, string>> parameters,
	                                         WWWCallback callback)
	{
		WWWForm form = null;
		if (parameters != null) {
			form = new WWWForm ();
			foreach (var parameter in parameters)
				form.AddField (parameter.Key, parameter.Value);
		}
		var www = form == null ? new WWW (url) : new WWW (url, form);
		yield return www;
		
		if (!www.isDone || !string.IsNullOrEmpty (www.error))
			Handle (null, www.error, callback);
		else
			Handle (www.text, www.error, callback);
	}
	
	private static void Handle (string response, string error, WWWCallback callback)
	{
		if (callback == null)
			return;
		
		if (!string.IsNullOrEmpty (error)) {
			callback (new Result<string> (null, error));
			return;
		}
		
		callback (new Result<string> (response));
	}
	
	private static readonly List<Action> actions = new List<Action> ();
	
	public static void QueueOnMainThread (Action action)
	{
		lock (actions) {
			actions.Add (action);
		}
	}
	
	public static Thread RunAsync (Action a)
	{        
		while (numThreads >= maxThreads)
			Thread.Sleep (1);
		Interlocked.Increment (ref numThreads);
		ThreadPool.QueueUserWorkItem (RunAction, a);
		return null;
	}
	
	private static void RunAction (object action)
	{
		try {
			((Action)action) ();
		} finally {
			Interlocked.Decrement (ref numThreads);
		}
	}
	
	private void Update ()
	{
		if (actions.Count <= 0)
			return;
		Action[] tempActions = null;
		lock (actions) {
			tempActions = actions.ToArray ();
			actions.Clear ();
		}
		foreach (var action in tempActions)
			action ();
	}
}