// NOTE DONT put in an editor folder

using UnityEngine;

public class StatsBarAttribute : PropertyAttribute
{
    public string valueMax;
    public StatsBarColor color;

    public StatsBarAttribute(string valueMax = null, StatsBarColor color = StatsBarColor.Red)
    {
        this.valueMax = valueMax;
        this.color = color;
    }
}

public enum StatsBarColor
{
    Red,
    Pink,
    Orange,
    Yellow,
    Green,
    Blue,
    Indigo,
    Violet,
    White
}