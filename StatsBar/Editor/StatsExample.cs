// Test compoent

using UnityEngine;
using System.Collections;

public class StatsExample : MonoBehaviour
{
    // Apply the StatsBar property Drawer to out property. Were passing "statMax" into the attribute.
    // This is the name of the property that holds the maximum value the stat property can hold. The
    // Drawer can then use this to look up the max value and get percentages correct. Because the max
    // value is backed by a property, it can change whilst our game is running (like, say we grow a level)!
    [StatsBar("statMax", StatsBarColor.Red)]
    public float health = 25;
    [StatsBar("statMax", StatsBarColor.Blue)]
    public float mana = 50;
    [StatsBar("statMax", StatsBarColor.Yellow)]
    public float strength = 90;

    public float statMax = 100;

    void Start () {
        health = 100;
        mana = 100;
        strength = 100;
    }
    
    void Update ()
    {
        health -= Time.deltaTime;
        mana -= Time.deltaTime*4;
        strength -= Time.deltaTime*8;

        if (health < 25)
            health = 100;

        if (mana < 25)
            mana = 100;

        if (strength < 25)
            strength = 100;
    }
}