using UnityEngine;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

public static class Vector3Ext {
	
	public static Vector3 RotateAround(this Vector3 v, Vector3 pivot, float angle) {
		
		Quaternion r = Quaternion.AngleAxis(angle, Vector3.up);
		
		return r * v;
		
	}

#if UNITY_4_7
    public static Vector3 LerpUnclamped(this Vector3 from, Vector3 to, float time)
    {
        return from + (to - from)*time;
    }

    public static Color LerpUnclamped(this Color from, Color to, float time)
    {
        return from + (to - from)*time;
    }
#endif

}
