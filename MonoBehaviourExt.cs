using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;

public class MonoBehaviourExt : MonoBehaviour /*, System.ICloneable*/
{
    [Flags]
    public enum ParentType
    {
        Default = 0,

        NoScale = 0x1,
        NoRotation = 0x2,
        NoPosition = 0x4,

        NoRotationAndPosition = NoRotation | NoPosition,
        NoScaleAndPosition = NoScale | NoPosition,
        NoRotationAndScale = NoRotation | NoScale,
    };
	
    public virtual Bounds GetBoundsUnity()
    {
        return new Bounds();
    }

    public void SetParent(Transform parent, ParentType parentType = ParentType.Default)
    {
        transform.parent = parent;

        if (parentType == ParentType.Default || (parentType & ParentType.NoPosition) == 0)
            transform.localPosition = Vector3.zero;
        if (parentType == ParentType.Default || (parentType & ParentType.NoRotation) == 0)
            transform.localRotation = Quaternion.identity;
        if (parentType == ParentType.Default || (parentType & ParentType.NoScale) == 0)
            transform.localScale = Vector3.one;
    }

    private bool fixedTimeInited;
    private float lastFixedTime;

    public float fixedTime
    {
        get
        {
            float val = 0f;

            if (fixedTimeInited == false)
            {
                val = 0f;
                lastFixedTime = Time.realtimeSinceStartup;
                fixedTimeInited = true;
            }
            else
            {
                val = Time.realtimeSinceStartup - lastFixedTime;
                lastFixedTime = Time.realtimeSinceStartup;
            }

            return val;
        }
    }

    public void WaitAndCall(float time, Action callback)
    {
        StartCoroutine(CallTimed(time, callback));
    }

    private IEnumerator CallTimed(float time, Action callback)
    {
        yield return new WaitForSeconds(time);
        callback.CallCallback();
    }


    public IEnumerator WaitForSecondsFixed(float time)
    {
        fixedTimeInited = false;
        float startTime = 0f;
        while (startTime < time)
        {
            startTime += fixedTime;
            yield return false;
        }
    }

    #region EVENTS

    public event Action<MonoBehaviourExt> OnDestroyEvent;

    public delegate void EventHandlerObject<T>(T sender) where T : MonoBehaviourExt;

    public void OnDestroy() {
        if (Application.isPlaying == false) return;
        OnDestroyEvent.CallCallback(this);
    }

    #endregion

    #region CLONE

#if UNITY_4_7 || UNITY_4_6
    public static T Instantiate<T>(T source) where T : MonoBehaviourExt
    {
        return INTERNAL_Clone(source);
    }
#endif

    public static new T Instantiate<T>(T source, Vector3 position, Quaternion rotation) where T : MonoBehaviourExt
    {
        T clone = INTERNAL_Clone(source);
        clone.transform.position = position;
        clone.transform.rotation = rotation;

        return clone;
    }

    private static T INTERNAL_Clone<T>(T source) where T : MonoBehaviourExt
    {
        return Object.Instantiate(source) as T;
    }

    #endregion

    

    public Vector3 position
    {
        get { return transform.position; }
    }

    public Vector3 localPosition
    {
        get { return transform.localPosition; }
        set { transform.localPosition = value; }
    }


    public Vector3 localEulerAngles
    {
        get { return transform.localEulerAngles; }
        set { transform.localEulerAngles = value; }
    }

    #region COLLIDER

    public Bounds bounds
    {
		get { return GetComponent<Collider>().bounds; }
    }

    #endregion
}