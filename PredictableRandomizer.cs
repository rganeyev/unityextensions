﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PredictableRandomizer : InstanceBehaviour {
    private static Dictionary<string, List<int>> randomIntegers;
    private bool seedApplied;

    public int seedToSetup = 256;
    private readonly bool useSeed = true;

    public override void Awake()
    {
        base.Awake();

        if (useSeed && !seedApplied) {
            SetSeed(seedToSetup);
            seedApplied = true;
        }
    }

    public static void SetSeed(int seed)
    {
#if UNITY_5
        Random.InitState(seed);
#else
        Random.seed = seed;
#endif
    }

    public static int RandomRange(int min, int max)
    {
        return Random.Range(min, max);
    }

    public static int RandomRange(int min, int max, string key, int allow = 1)
    {
        if (randomIntegers == null)
            randomIntegers = new Dictionary<string, List<int>>();

        List<int> rowList;

        var val = Random.Range(min, max);
        if (randomIntegers.ContainsKey(key)) {
            rowList = randomIntegers[key];
            var row = 0;
            foreach (var value in rowList) {
                if (val == value) {
                    row++;
                }
            }

            if (row > allow) {
                val += 1;
                if (val >= max)
                    val = min;

                rowList.Clear();
            }

            rowList.Add(val);
            randomIntegers.Remove(key);
        } else {
            rowList = new List<int> {val};
        }

        randomIntegers.Add(key, rowList);
        return val;
    }

    public static float RandomRange(float min, float max)
    {
        return Random.Range(min, max);
    }
}