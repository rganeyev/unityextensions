﻿using System;
using System.Collections.Generic;
using System.IO;
using LaikaBOSS.MiniJSON;
using UnityEngine;
using System.Text;
using System.Security.Cryptography;
using CodeStage.AntiCheat.ObscuredTypes;

public static class StringExtensions
{
	public static Dictionary<string, object> DeserializeToDictionary(this string str)
	{
		return Deserialize<Dictionary<string, object>>(str);
	}

	public static T Deserialize<T>(this string str) where T : class
	{
		return Json.Deserialize(str) as T;
	}

	public static string PlusPercent(this float value)
	{
		return "+" + (Mathf.RoundToInt((value - 1)*100)) + "%";
	}

	public static string PlusPercent(this ObscuredFloat value)
	{
		return "+" + (Mathf.RoundToInt((value - 1)*100)) + "%";
	}

	public static void WriteAllLines(this IEnumerable<string> lines, string filename)
	{
		using (var writer = new StreamWriter(filename))
		{
			foreach (var line in lines)
				writer.WriteLine(line);
		}
	}
	
	public static string SubstringFromOccurence(this string str, string pattern)
	{
		int index = str.IndexOf(pattern, StringComparison.Ordinal);
		return index > 0 ? str.Substring(index) : string.Empty;
	}

	public static string SubstringToOccurence(this string str, string pattern)
	{
		int index = str.IndexOf(pattern, StringComparison.Ordinal);
		return index > 0 ? str.Substring(0, index) : str;
	}

	public static string MD5Hash(this string input)
	{
		MD5 md5Hash = MD5.Create();
		byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
		var sBuilder = new StringBuilder();

		foreach (var bt in data)
			sBuilder.Append(bt.ToString("x2"));

		return sBuilder.ToString();
	}

	public static string Translit(this string str)
	{
		var result = str;
		string[] lat_up = {"A", "B", "V", "G", "D", "E", "Yo", "Zh", "Z", "I", "Y", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "Kh", "Ts", "Ch", "Sh", "Shch", "\"", "Y", "'", "E", "Yu", "Ya"};
		string[] lat_low = {"a", "b", "v", "g", "d", "e", "yo", "zh", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "ts", "ch", "sh", "shch", "\"", "y", "'", "e", "yu", "ya"};
		string[] rus_up = {"А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я"};
		string[] rus_low = { "а", "б", "в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я"};
		for (int i = 0; i <= 32; i++)
		{
			result = result.Replace(rus_up[i],lat_up[i]);
			result = result.Replace(rus_low[i],lat_low[i]);              
		}
		return result;
	}

    public static string EnglishNumber(this long num)
    {
        if (Application.systemLanguage == SystemLanguage.English)
        {
            if (num == 1)
                return "1st";
            if (num == 2)
                return "2nd";
            if (num == 3)
                return "3nd";
            return num + "th";
        }
        return num.ToString();
    }

}
