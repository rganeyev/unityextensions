using UnityEngine;

public static class GameObjectExt {
    public static void SetLayers(this GameObject parent, int layer) {
        Transform tParent = parent.transform;
        foreach (var t in tParent.GetComponentsInChildren<Transform>(true)) t.gameObject.SetLayer(layer);
    }

    public static void SetLayer(this GameObject go, int layer) {
        go.layer = layer;
    }

    public static void SetLayer(this GameObject go, string name) {
        go.layer = LayerMask.NameToLayer(name);
    }

public static void RemoveChildren(this Transform transform)
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
            Object.Destroy(transform.GetChild(i).gameObject);
    }

	public static void SetParent(this GameObject go, GameObject parent)
	{
        if (parent != null)
        {
            Transform t = go.transform;
            t.SetParent(parent.transform);
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localScale = Vector3.one;
            go.layer = parent.layer;
        }
    }

	public static void SetParent(this MonoBehaviour mb, Transform parent, MonoBehaviourExt.ParentType parentType = MonoBehaviourExt.ParentType.Default)
	{
		mb.transform.parent = parent;
		if (parentType == MonoBehaviourExt.ParentType.Default || (parentType & MonoBehaviourExt.ParentType.NoPosition) == 0)
			mb.transform.localPosition = Vector3.zero;
		if (parentType == MonoBehaviourExt.ParentType.Default || (parentType & MonoBehaviourExt.ParentType.NoRotation) == 0)
			mb.transform.localRotation = Quaternion.identity;
		if (parentType == MonoBehaviourExt.ParentType.Default || (parentType & MonoBehaviourExt.ParentType.NoScale) == 0)
			mb.transform.localScale = Vector3.one;
	}
}