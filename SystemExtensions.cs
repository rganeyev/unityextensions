﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;
using Random = UnityEngine.Random;

public static class SystemExtensions
{
	public static void CallCallback<T> (this Action<T> callback, T result)
	{
		if (callback != null)
			callback.Invoke (result);
	}

	public static void CallCallback<T1, T2> (this Action<T1, T2> callback, T1 result1, T2 result2)
	{
		if (callback != null)
			callback.Invoke (result1, result2);
	}

	public static void CallCallback (this Action callback)
	{
		if (callback != null)
			callback.Invoke ();
	}

	public static Type GetEnumeratedType<T> (this IEnumerable<T> _)
	{
		return typeof(T);
	}

	public static T GetRandomValue<T> (this T[] array)
	{
		return array [Random.Range (0, array.Length)];
	}

	public static bool CheckAndAdd<T, V> (this Dictionary<T, V> dictionary, T key, V value)
	{
		if (dictionary.ContainsKey (key)) {
			dictionary [key] = value;
			return false;
		}
		dictionary.Add (key, value);
		return true;
	}

	public static int GetRandom (int[] probabilities)
	{
		var sum = 0;
		foreach (var val in probabilities)
			sum += val;
		var rand = Random.Range (0, sum);
		sum = 0;
		for (var i = 0; i < probabilities.Length; ++i) {
			sum += probabilities [i];
			if (rand < sum)
				return i;
		}
		return probabilities.Length - 1;
	}

	public static float Truncate (this float number, int presicion)
	{
		var trunc = (int)Mathf.Pow (10, presicion);
		return Mathf.Round (number * trunc) / trunc;
	}

	public static DateTime ChangeTime (this DateTime dateTime, int hours, int minutes, int seconds, int milliseconds)
	{
		return new DateTime (
			dateTime.Year,
			dateTime.Month,
			dateTime.Day,
			hours,
			minutes,
			seconds,
			milliseconds,
			dateTime.Kind);
	}

	//works only with [Serializable] attribute
	//if you need more generic, use http://stackoverflow.com/questions/129389/how-do-you-do-a-deep-copy-an-object-in-net-c-specifically
	public static T DeepClone<T> (this T obj)
	{
		using (var ms = new MemoryStream ()) {
			var formatter = new BinaryFormatter ();
			formatter.Serialize (ms, obj);
			ms.Position = 0;

			return (T)formatter.Deserialize (ms);
		}
	}

	public static string RegExpReplace (this string text, string pattern, string replace)
	{
		var regex = new Regex (pattern);
		return regex.Replace (text, replace);
	}

	public static string FormatInteger (this int value)
	{
		var lengthValue = Mathf.Clamp (value, 0, int.MaxValue);
		if (lengthValue == 0)
			return lengthValue.ToString ();
		return lengthValue.ToString ("### ### ###").Trim ();
	}

	public static string FormatFloat (this float value)
	{
		var lengthValue = Mathf.Clamp (value, 0, float.MaxValue);
		return lengthValue.ToString ("n1").Trim ();
	}

	public static string FormatFloat (this ObscuredFloat value)
	{
		return FormatFloat ((float)value);
	}

	public static string FormatInteger (this ObscuredInt val)
	{
		int value = val;
		var lengthValue = Mathf.Clamp (value, 0, int.MaxValue);
		if (lengthValue == 0)
			return lengthValue.ToString ();
		return lengthValue.ToString ("### ### ###").Trim ();
	}


	public static ObscuredInt Clamp (this ObscuredInt val, ObscuredInt start, ObscuredInt end)
	{
		if (val < start)
			return start;
		if (val > end)
			return end;
		return val;
	}

	public static string FormatHourTime (this TimeSpan timeSpan)
	{
		return string.Format ("{0}:{1}:{2}", timeSpan.Hours.ToString ("D2"),
			timeSpan.Minutes.ToString ("D2"), timeSpan.Seconds.ToString ("D2"));
	}

	public static string FormatMinuteTime (this TimeSpan timeSpan)
	{
		return string.Format ("{0}:{1}", timeSpan.Minutes.ToString ("D2"), timeSpan.Seconds.ToString ("D2"));
	}

	public static string FormatHourTime (this float value)
	{
		int minutes = ((int)value) / 60;
		var seconds = (int)value % 60;

		var millisec = (int)((value - (int)value) * 100);
		return string.Format ("{0:D2}:{1:D2}:{2:D2}", minutes, seconds, millisec);
	}

	public static string FormatMinuteTime (this float value)
	{
		int tMin = ((int)value) / 60;
		int tSec = ((int)value) % 60;

		return string.Format ("{0:D2}:{1:D2}", tMin, tSec);		       
	}

	public static float mapRange (float fromA1, float fromA2, float toB1, float toB2, float value)
	{
		return toB1 + (value - fromA1) * (toB2 - toB1) / (fromA2 - fromA1);
	}

	public static bool IsGenericList (this Type type)
	{
		foreach (Type @interface in type.GetInterfaces()) {
			if (@interface.IsGenericType && @interface.GetGenericTypeDefinition () == typeof(IList<>))
				return true;
		}
		return false;
	}

	public static bool IsNullOrEmpty<T> (this IList<T> list)
	{
		return list == null || list.Count == 0;
	}
}