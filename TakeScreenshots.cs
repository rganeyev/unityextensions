﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using System.IO;


public class ScreenshotTaker : MonoBehaviour {
#if UNITY_EDITOR
	public void Take (List<Vector2> dimensions, bool horizontal = true)
	{
		int was = QualitySettings.antiAliasing;
		QualitySettings.antiAliasing = 8;

		StartCoroutine(row(dimensions, horizontal));

		QualitySettings.antiAliasing = was;
	}

	IEnumerator row(List<Vector2> dimensions, bool horizontal)
	{
		Time.timeScale = 0.01f;

		foreach (var dw in dimensions)
		{
			yield return new WaitForSeconds(0.003f);

			if (horizontal)
				StartCoroutine (TakeScreenshot((int)dw.x, (int)dw.y));
			else
				StartCoroutine (TakeScreenshot((int)dw.y, (int)dw.x));

			yield return new WaitForSeconds(0.003f);
		}

		yield return new WaitForSeconds(0.005f);
		setWindowResolution(640, 480);

		Time.timeScale = 1;
		Destroy(this.gameObject);
	}
	
	IEnumerator TakeScreenshot (int resWidth, int resHeight)
	{
		Debug.LogWarning("TakeScreenshot " + resWidth + " " + resHeight);
		setWindowResolution(resWidth, resHeight);
		
		//float time = 1;
		//float start = Time.realtimeSinceStartup;
		
		yield return new WaitForSeconds(0.002f);

		string fileName = "image_" + resWidth + "x" + resHeight + ".png";
		Application.CaptureScreenshot(fileName);
		string path = System.IO.Path.Combine (Application.persistentDataPath, fileName);
		
		if(File.Exists(path)) 
		{
			// TODO: new system needed
			//WWW loadedImage = new WWW("file://" + path);
		}
	}
	
	static void setWindowResolution(int resWidth, int resHeight)
	{
		Rect R = GetMainGameView().position;
		R.width = resWidth;
		R.height = resHeight + 17;
		R.position = new Vector2(100,100);

		Debug.LogWarning("Size " + R.width + " " + R.height);

		GetMainGameView().position = R;
		GetMainGameView().autoRepaintOnSceneChange = true;
		GetMainGameView().Repaint();
		
		GetMainGameView().Focus();
	}
	
	public static EditorWindow GetMainGameView()
	{
		System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
		System.Reflection.MethodInfo GetMainGameView = T.GetMethod("GetMainGameView",System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
		System.Object Res = GetMainGameView.Invoke(null,null);
		return (EditorWindow)Res;
	}
#endif
}
